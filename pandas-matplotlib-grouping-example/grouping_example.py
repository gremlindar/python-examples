import matplotlib.pyplot as plt
import pandas as pd

biostats = pd.read_csv("biostats.csv")
gender_groups = biostats.groupby(["Gender"])
figure, axes = plt.subplots(nrows=1, ncols=len(gender_groups.groups.keys()))
for (key, ax) in zip(gender_groups.groups.keys(), axes.flatten()):
    gender_groups.get_group(key).plot(ax=ax)
    ax.legend()
    ax.set_title(key)
plt.show()
